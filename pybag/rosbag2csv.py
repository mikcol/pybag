try:
    import rosbag
except ImportError:
    print('Cannot import rosbag, run the docker version')

import argparse
import sys

if sys.version_info.major < 3:
    import pathlib2 as pathlib
else:
    import pathlib

from .util import (get_parser_dict, create_data_with_header,
                   create_folder_structure, save_image)


def export_images(input_bag, output_folder, topics=None):

    root_path = pathlib.Path(output_folder).absolute()
    with rosbag.Bag(input_bag, 'r') as bag:
        topics = bag.get_type_and_topic_info()
        image_topics = [topic for topic, val in topics.topics.items()
                        if val.msg_type == 'sensor_msgs/CompressedImage']
        print('Image Topics: {}'.format(image_topics))

        assert isinstance(image_topics, list)

        index = 0
        for topic, msg, time in bag.read_messages():
            if topic == image_topics[0]:
                try:
                    save_image(index, msg, root_path)
                except Exception as ex:
                    print(ex)
                index += 1


def export_bag(input_bag, output_folder, topics=None):

    root_path = pathlib.Path(output_folder).absolute()
    with rosbag.Bag(input_bag, 'r') as bag:

        bag_info = bag.get_type_and_topic_info()
        if not topics:
            topics = bag_info.topics.keys()

        assert isinstance(topics, list)
        data_parser = get_parser_dict(bag_info)
        data = create_data_with_header(bag_info, data_parser)
        path_dict = create_folder_structure(root_path, data.keys())

        for topic, msg, time in bag.read_messages(topics=topics):
            try:
                data[topic].append(data_parser[msg._type](msg, time))
            except KeyError:
                pass

        for topic, val in data.items():
            name = path_dict[topic]['name']
            path = path_dict[topic]['path']

            outfile = (path / name).with_suffix('.csv')

            with outfile.open('w', encoding='utf-8') as fout:
                for line in val:
                    try:
                        fout.write(unicode(line, 'utf-8'))
                    except TypeError:
                        print(line)
                        print(type(line))


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='ROSBAG exporter scrtip')
    parser.add_argument('bagfile', type=str,
                        help='rosbag input file to be exported')

    parser.add_argument('output', type=str, help='name of output')
    args = parser.parse_args()

    print(args)
    export_bag(args.bagfile, args.output)
