from .parsers import global_message_handler
import docker
import sys
if sys.version_info.major < 3:
    import pathlib2 as pathlib
else:
    import pathlib
import logging
import getpass
import numpy as np
import uuid


DOCKERFILEPATH = pathlib.Path(__file__).parent / 'docker'
DOCKER_TAG = 'pybag:deploy'


def save_image(index, img, outputdir):
    try:
        import cv2
    except ImportError as ex:
        logging.error('Failed import of opencv: {}'.format(ex))
        return None
    img_arr = np.fromstring(img.data, np.uint8)
    cv_image = cv2.imdecode(img_arr, cv2.IMREAD_COLOR)
    index_s = str(index).zfill(6)
    fname = index_s + hex(uuid.uuid1().int >> 64) + '.png'
    outputfile = outputdir / fname
    try:
        cv2.imwrite(str(outputfile), cv_image)
    except Exception as ex:
        print(ex)
        exit(0)


def run_docker_instance(bagfile, outputdir, reset_image=False,
                        image_data=False):

    bagfile = pathlib.Path(bagfile).resolve()
    outputdir = pathlib.Path(outputdir).resolve()

    try:
        outputdir.mkdir()
    except OSError:
        logging.error('Output path already exists')

    user = getpass.getuser()
    if not outputdir.owner() == user:
        raise OSError('{} is owned by {}'.format(str(outputdir),
                                                 outputdir.owner()))

    client = docker.from_env()
    images = client.images.list()

    try:
        tags = [image.tags[0] for image in images]
    except IndexError:
        logging.info('No images available')
        tags = []
    if DOCKER_TAG not in tags or reset_image:
        logging.info('Creating Docker Image with tag: %s' % DOCKER_TAG)
        client.images.build(path=str(DOCKERFILEPATH), tag=DOCKER_TAG)

    bagfile_path = bagfile.parent
    volumes = {str(bagfile_path): {'bind': '/home/pyuser/data', 'mode': 'ro'},
               str(outputdir): {'bind': '/home/pyuser/output', 'mode': 'rw'}}

    bagfile_name = bagfile.name
    if image_data:
        command = 'python -m pybag /home/pyuser/data/%s /home/pyuser/output --images' % bagfile_name
    else:
        command = 'python -m pybag /home/pyuser/data/%s /home/pyuser/output' % bagfile_name
    output = client.containers.run(DOCKER_TAG, command, volumes=volumes,
                                   remove=True)
    logging.info(output)


def get_parser_dict(bag_info):

    msg_types = bag_info.msg_types.keys()

    parser_dict = {}
    for msg_type in msg_types:
        try:
            parser_dict[msg_type] = global_message_handler[msg_type](string=True)
        except KeyError:
            logging.info('%s does not exist in the global message handler' % msg_type)
    return parser_dict


def create_data_with_header(bag_info, data_parser):

    topics = bag_info.topics
    data = {}
    for topic, val in topics.items():
        try:
            data[topic] = [data_parser[val.msg_type].header]
        except KeyError:
            logging.info('%s does not exist' % topic)
    return data


def create_folder_structure(root_folder, topics):

    paths = {}
    topics = sorted(topics)
    for topic in topics:
        filename = root_folder / topic.lstrip('/')
        path = filename.parent
        try:
            path.mkdir(parents=True)
        except OSError:
            logging.info('Path %s already exists' % path)
        finally:
            paths[topic] = {'name': str(filename.name), 'path': path}

    return paths


def decompose(msg):

    print(dir(msg))
    print(msg.__slots__)
    print(msg._type)
    for slot in msg.__slots__:

        if slot is 'transforms':
            variable = getattr(msg, slot)
            print(variable)
            print(len(variable))
        if slot is not 'header' and 'covariance' not in slot:
            variable = getattr(msg, slot)
            try:
                print(dir(variable))
                print(variable.__slots__)
            except AttributeError:
                print('%s has no __slots__' % slot)
