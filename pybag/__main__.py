import argparse
import sys
if sys.version_info.major < 3:
    from .rosbag2csv import export_bag, export_images
import logging
logging.basicConfig(filename='pybag.log', filemode='w', level=logging.DEBUG)


def main(args):

    if args.docker:
        try:
            from .util import run_docker_instance
        except ImportError:
            logging.error("Failed to import docker")
        run_docker_instance(args.bagfile, args.output, reset_image=True,
                            image_data=args.images)
    elif args.images:
        export_images(args.bagfile, args.output)
    else:
        export_bag(args.bagfile, args.output)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='ROSBAG exporter scrtip')
    parser.add_argument('bagfile', type=str,
                        help='rosbag input file to be exported')
    parser.add_argument('output', type=str, help='name of output')
    parser.add_argument('--docker', default=False, action='store_true')
    parser.add_argument('--images', default=False, action='store_true')
    args = parser.parse_args()

    main(args)
    # export_bag(args.bagfile, args.output)
