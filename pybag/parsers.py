class TemplateMeta(type):
    def __new__(cls, name, bases, body):
        if 'parse' not in body:
            raise TypeError('Class %s does not contain parser function' % name)
        elif body['parse'].__code__.co_argcount != 2:
            raise TypeError('parse-function of class %s should contain message and time arguments' % name)
        if 'mheader' not in body:
            raise TypeError('Class %s does not contain mheader function' % name)
        return type.__new__(cls, name, bases, body)


class ParserTemplate(object):
    __metaclass__ = TemplateMeta

    def __init__(self, string=False):
        self._string = string

    @property
    def mheader(self):
        return None

    @property
    def header(self):
        return 'time,' + self.mheader

    def parse(self, msg):
        raise NotImplementedError()

    def __call__(self, msg, time):

        result = [self.parse_time(msg, time)]
        result.extend(self.parse(msg))
        return self.return_message(result)

    def get_time_stamp(self, msg):

        return msg.header.stamp

    def parse_time(self, msg, time):
        try:
            parsed_time = parse_time(self.get_time_stamp(msg.header.stamp))
        except AttributeError:
            parsed_time = parse_time(time)

        return parsed_time

    def return_message(self, msg):

        if self._string:
            return ','.join(str(x) for x in msg) + '\n'
        else:
            return msg


class Test(ParserTemplate):

    @property
    def mheader(self):
        return 'test1,test2,test3\n'

    def parse(self, msg):

        return [1, 2, 3, 4]


class WrenchStampedParser(ParserTemplate):

    @property
    def mheader(self):
        return 'force_x,force_y,force_z,torque_x,torque_y,torque_z\n'

    def parse(self, msg):

        force_x = msg.wrench.force.x
        force_y = msg.wrench.force.y
        force_z = msg.wrench.force.z

        torque_x = msg.wrench.torque.x
        torque_y = msg.wrench.torque.y
        torque_z = msg.wrench.torque.z
        message = [force_x, force_y, force_z,
                   torque_x, torque_y, torque_z]
        return message


class OdometryParser(ParserTemplate):

    @property
    def mheader(self):
        return 'x,y,z,quat_x,quat_y,quat_z,quat_w,u,v,w,p,q,r\n'

    def parse(self, msg):

        x = msg.pose.position.x
        y = msg.pose.position.y
        z = msg.pose.position.z

        quat_x = msg.pose.orientation.x
        quat_y = msg.pose.orientation.y
        quat_z = msg.pose.orientation.z
        quat_w = msg.pose.orientation.w
        u = msg.wrench.torque.x
        v = msg.wrench.torque.y
        w = msg.wrench.torque.z

        p = msg.wrench.torque.x
        q = msg.wrench.torque.y
        r = msg.wrench.torque.z
        message = [x, y, z, quat_x, quat_y, quat_z, quat_w,
                   u, v, w, p, q, r]
        return message


class WrenchParser(ParserTemplate):

    @property
    def mheader(self):
        return 'force_x,force_y,force_z,torque_x,torque_y,torque_z\n'

    def parse(self, msg):

        force_x = msg.force.x
        force_y = msg.force.y
        force_z = msg.force.z

        torque_x = msg.torque.x
        torque_y = msg.torque.y
        torque_z = msg.torque.z
        # message_time = self.parse_time(msg, time)
        message = [force_x, force_y, force_z,
                   torque_x, torque_y, torque_z]

        return message


class TFParser(ParserTemplate):

    def get_time_stamp(self, msg):
        return msg.transforms[0].header.stamp

    @property
    def mheader(self):
        return 'pos_x,pos_y,pos_z,quat_x,quat_y,quat_z,quat_w\n'

    def parse(self, msg):

        msg_tf = msg.transforms[0]
        pos_x = msg_tf.transform.translation.x
        pos_y = msg_tf.transform.translation.y
        pos_z = msg_tf.transform.translation.z

        quat_x = msg_tf.transform.rotation.x
        quat_y = msg_tf.transform.rotation.y
        quat_z = msg_tf.transform.rotation.z
        quat_w = msg_tf.transform.rotation.w
        message = [pos_x, pos_y, pos_z,
                   quat_x, quat_y, quat_z, quat_w]

        return message


class QuaternionStampedParser(ParserTemplate):

    @property
    def mheader(self):
        return 'quat_x,quat_y,quat_z,quat_w\n'

    def parse(self, msg):

        quat_x = msg.quaternion.x
        quat_y = msg.quaternion.y
        quat_z = msg.quaternion.z
        quat_w = msg.quaternion.w
        message = [quat_x, quat_y, quat_z, quat_w]

        return message


class Vector3StampedParser(ParserTemplate):

    @property
    def mheader(self):
        return 'x,y,z\n'

    def parse(self, msg):

        x = msg.vector.x
        y = msg.vector.y
        z = msg.vector.z
        message = [x, y, z]

        return message


class DVLParser(ParserTemplate):

    @property
    def mheader(self):
        return 'x,y,z,altitude\n'

    def parse(self, msg):

        x = msg.velocity.x
        y = msg.velocity.y
        z = msg.velocity.z
        altitude = msg.altitude
        message = [x, y, z, altitude]

        return message


class TwistStampedParser(ParserTemplate):

    @property
    def mheader(self):
        return 'lin_x,lin_y,lin_z,angular_x,angular_y,angular_z\n'

    def parse(self, msg):

        lin_x = msg.twist.linear.x
        lin_y = msg.twist.linear.y
        lin_z = msg.twist.linear.z

        angular_x = msg.twist.angular.x
        angular_y = msg.twist.angular.y
        angular_z = msg.twist.angular.z

        message = [lin_x, lin_y, lin_z,
                   angular_x, angular_y, angular_z]
        return message


class TwistWithCovarianceStampedParser(ParserTemplate):

    @property
    def mheader(self):
        return 'lin_x,lin_y,lin_z,angular_x,angular_y,angular_z\n'

    def parse(self, msg):

        lin_x = msg.twist.twist.linear.x
        lin_y = msg.twist.twist.linear.y
        lin_z = msg.twist.twist.linear.z

        angular_x = msg.twist.twist.angular.x
        angular_y = msg.twist.twist.angular.y
        angular_z = msg.twist.twist.angular.z

        message = [lin_x, lin_y, lin_z,
                   angular_x, angular_y, angular_z]
        return message


class IMUParser(ParserTemplate):

    @property
    def mheader(self):
        return 'acc_x,acc_y,acc_z,gyro_x,gyro_y,gyro_z\n'

    def parse(self, msg):

        acc_x = msg.linear_acceleration.x

        acc_y = msg.linear_acceleration.y
        acc_z = msg.linear_acceleration.z

        gyro_x = msg.angular_velocity.x
        gyro_y = msg.angular_velocity.y
        gyro_z = msg.angular_velocity.z
        message = [acc_x, acc_y, acc_z,
                   gyro_x, gyro_y, gyro_z]

        return message


class BatteryStateParser(ParserTemplate):

    @property
    def mheader(self):
        return 'voltage\n'

    def parse(self, msg):

        voltage = msg.voltage
        # current = msg.current

        return [voltage]


class FloatParser(ParserTemplate):

    @property
    def mheader(self):
        return 'float\n'

    def parse(self, msg):

        data = msg.data

        return [data]


class RangeParser(ParserTemplate):

    @property
    def mheader(self):
        return 'radiation_type,field_of_view,min_range,max_range,range\n'

    def parse(self, msg):

        radiation_type = msg.radiation_type
        field_of_view = msg.field_of_view
        min_range = msg.min_range
        max_range = msg.max_range
        range_var = msg.range

        return [radiation_type, field_of_view, min_range, max_range, range_var]


class ADCMessageParser(ParserTemplate):

    @property
    def mheader(self):
        var_names = ['voltage' + str(x) for x in range(6)]
        return ','.join(var_names) + '\n'

    def parse(self, msg):

        voltage = msg.voltage

        return voltage


class FluidPressureParser(ParserTemplate):

    @property
    def mheader(self):
        return 'fluid_pressure,variance\n'

    def parse(self, msg):

        fluid_pressure = msg.fluid_pressure
        variance = msg.variance

        return [fluid_pressure, variance]


class PWMCommandsParser(ParserTemplate):

    @property
    def mheader(self):
        var_names = ['pwm' + str(x) for x in range(14)]
        return ','.join(var_names) + '\n'

    def parse(self, msg):

        pwm = msg.pwm

        return pwm


class BoolParser(ParserTemplate):

    @property
    def mheader(self):
        return 'bool\n'

    def parse(self, msg):

        data = msg.data

        return [data]


class ArmedParser(ParserTemplate):

    @property
    def mheader(self):
        return 'armed\n'

    def parse(self, msg):

        armed = msg.armed

        return [armed]


def parse_time(rostime):

    return rostime.secs + (rostime.nsecs / 1000000000.0)


global_message_handler = {
    'geometry_msgs/QuaternionStamped': QuaternionStampedParser,
    'geometry_msgs/TwistStamped': TwistStampedParser,
    'geometry_msgs/TwistWithCovarianceStamped': TwistWithCovarianceStampedParser,
    'geometry_msgs/Vector3Stamped': Vector3StampedParser,
    'geometry_msgs/Wrench': WrenchParser,
    'geometry_msgs/WrenchStamped': WrenchStampedParser,
    'nav_msgs/Odometry': OdometryParser,
    # 'rosgraph_msgs/Clock': Test,
    # 'rosgraph_msgs/Log': Test,
    'seavention_msgs/Armed': ArmedParser,
    'seavention_msgs/PWMCommands': PWMCommandsParser,
    'seavention_msgs/BatteryState': BatteryStateParser,
    'seavention_msgs/ADCMessage': ADCMessageParser,
    'sensor_msgs/FluidPressure': FluidPressureParser,
    'sensor_msgs/Imu': IMUParser,
    # 'sensor_msgs/JointState': Test,
    # 'sensor_msgs/Joy': Test,
    # 'sensor_msgs/LaserScan': Test,
    'sensor_msgs/Range': RangeParser,
    'std_msgs/Bool': BoolParser,
    'std_msgs/Float64': FloatParser,
    # 'tf2_msgs/TFMessage': TFParser,
    'uuv_gazebo_ros_plugins_msgs/FloatStamped': FloatParser,
    'uuv_sensor_ros_plugins_msgs/DVL': DVLParser,
    # 'visualization_msgs/Marker': Test,
    # 'visualization_msgs/MarkerArray': Test
    }
