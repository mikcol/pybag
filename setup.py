from setuptools import setup

setup(
    name='pybag',
    version='0.1.0',
    description='A package to export rosbag files',
    author='Mikkel Cornelius Nielsen',
    author_email='mikkel.cornelius@gmail.com',
    packages=['pybag'],
    install_requires=['docker', 'pathlib2', 'pytest']
    )
